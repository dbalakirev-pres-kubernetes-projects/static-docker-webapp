# Static Docker Web Application

## Information
Based on *nginx*.

## Usage

### Build the image
docker build -t dbalakirev/static-docker-webapp[:tag] .

### Run the image
docker run -p 7000:80 dbalakirev/static-docker-webapp

### Use the app
Visit http://localhost:7000

## Publishing

### Login
docker login --username dbalakirev

### Push
docker push dbalakirev/static-docker-webap
